" Auto Install vim-plug
" Below scripts only works over 0.3.0 version of neovim!
" Need to check what version of neovim is."
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"****************************** [Plug in Manger : vim-plug] ******************************
set nocompatible
call plug#begin(stdpath('data') . '/plugged')

"++++++++++++++++++++++++++++++ [Essential] ++++++++++++++++++++++++++++++

" Intelligently reopen files at your last edit position
Plug 'farmergreg/vim-lastplace'
" Toggles between hybrid and absolute line numbers automatically
Plug 'jeffkreeftmeijer/vim-numbertoggle'
set number
" Delete/change/add parentheses/quotes/XML-tags/much more with ease
Plug 'tpope/vim-surround'

" A simple, easy-to-use Vim alignment plugin.
Plug 'junegunn/vim-easy-align'
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" Insert or delete brackets, parens, quotes in pair.
Plug 'jiangmiao/auto-pairs'
" Auto complete bracket, quotes
" Plug 'raimondi/delimitmate'


"++++++++++++++++++++++++++++++ [Color Scheme] ++++++++++++++++++++++++++++++

Plug 'nanotech/jellybeans.vim'
Plug 'morhetz/gruvbox'
Plug 'sainnhe/gruvbox-material'

Plug 'kaicataldo/material.vim', { 'branch': 'main' }
Plug 'sjl/badwolf'
Plug 'sainnhe/sonokai'
"Plug 'w0ng/vim-hybrid'
"Plug 'ayu-theme/ayu-vim' " or other package manager

" various languages enhanced syntax
Plug 'sheerun/vim-polyglot'

" Bracket Highlighter"
Plug 'luochen1990/rainbow'
let g:rainbow_active = 1

"++++++++++++++++++++++++++++++ [File Navigator] ++++++++++++++++++++++++++++++
"------------------------------ [Nerdtree]-------------------------------------

Plug 'preservim/nerdtree'
" mapping not recursively F9 to NerdtreeToogle  all mode except for INSERT Mode
noremap <silent> <F9> <Esc>:NERDTreeToggle<CR>
" NERDTree + Git
Plug 'Xuyuanp/nerdtree-git-plugin'

" NERDTree + Icon
Plug 'ryanoasis/vim-devicons'
" Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

"------------------------------ [FZF : Fuzzy finder] ++++++++++++++++++++++++++++++
Plug 'junegunn/fzf.vim'
Plug '~/.fzf'
" fzf keymap
nnoremap <silent> ,f :FZF<cr>
nnoremap <silent> ,F :FZF ~<cr>
" Buffer List
nnoremap <silent> ,bl :Buffers <cr>

"++++++++++++++++++++++++++++++ [Status Line] ++++++++++++++++++++++++++++++
"------------------------------ [VIM-AIRLINE]----------

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Airline diplays status of editor so don't need to showmode
" e.g. [-- INSERT --], [-- VISUAL --]
set noshowmode
set cmdheight=1
" use airline theme
let g:airline_theme='raven'
" use airline tabline extension : customized tab info
let g:airline#extensions#tabline#enabled = 1
" Only show file name
let g:airline#extensions#tabline#fnamemod = ':t'
"let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#show_tab_nr = 1
" not show tab count
let g:airline#extensions#tabline#show_tab_count = 0
" show buffer number
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#show_splits = 1
"let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
let g:airline#extensions#tabline#tabs_label = 't'
let g:airline#extensions#tabline#buffers_label = 'b'

"++++++++++++++++++++++++++++++ [Git Integration] ++++++++++++++++++++++++++++++

Plug 'tpope/vim-fugitive'
nnoremap <Leader>gs :Git<CR>
nnoremap <Leader>gl :Gclog<CR>
" Show only current file's log
nnoremap <Leader>gll :Gclog %<CR>
nnoremap <Leader>gvd :Gvdiffsplit<CR>
nnoremap <Leader>gb :Git blame<CR>

" highlight changing
Plug 'airblade/vim-gitgutter'

"++++++++++++++++++++++++++++++ [Programming Support] ++++++++++++++++++++++++++++++
"------------------------------ [Language Auto Complete & Code navigate ..]----------
" NOTE : required clangd
Plug 'neoclide/coc.nvim', {'branch': 'release'}
if !empty(glob("~/.config/nvim/coc-nvim.vim"))
	source ~/.config/nvim/coc-nvim.vim
endif

"------------------------------ [Tagbar]----------
Plug 'majutsushi/tagbar'
" when toggle tagbar the focus will move to tagbar window
let g:tagbar_autofocus = 1
" mapping only normal mode F8 to TagbarToggle
nnoremap <F8> :TagbarToggle<CR>

"------------------------------ [Cscope relate]----------
" use cscope easily
"Plug 'ronakg/quickr-cscope.vim'
Plug 'brookhong/cscope.vim'
nnoremap <leader>fa :call CscopeFindInteractive(expand('<cword>'))<CR>
nnoremap <leader>l :call ToggleLocationList()<CR>" s: Find this C symbol

"------------------------------ [Insert semicolon]----------
" Easily insert a semicolon at the end of sentence
Plug 'lfilho/cosco.vim'
"autocmd FileType javascript,css,c,cpp nmap <silent> <Leader>; <Plug>(cosco-commaOrSemiColon)
autocmd FileType javascript,css,c,cpp nmap <silent> ,; <Plug>(cosco-commaOrSemiColon)
autocmd FileType javascript,css,c,cpp imap <silent> <Leader>; <c-o><Plug>(cosco-commaOrSemiColon)

" Save file automatically
Plug '907th/vim-auto-save'
let g:auto_save = 1  " enable AutoSave on Vim startup
let g:auto_save_silent = 1  " do not display the auto-save notification
let g:auto_save_events = ["InsertLeave", "CursorHold"]

" Commentate easily
Plug 'preservim/nerdcommenter'
filetype plugin on
let g:NERDSpaceDelims = 1 " Add spaces after comment delimiters by default

" Ripgrep integrate
"Plug 'jremmen/vim-ripgrep'
Plug 'mileszs/ack.vim'
source ~/.config/nvim/ack.vim

"------------------------------ [MatchIt]----------
" extension of '%' command with XML, HTML, so on
" Plug 'vim-scripts/matchit.zip'
Plug 'andymass/vim-matchup'

"++++++++++++++++++++++++++++++ [Etc] ++++++++++++++++++++++++++++++
" For Yocto recipe file syntax (bb, bbapend, ..)
Plug 'kergoth/vim-bitbake'
" The Fancy start screen for vim
Plug 'mhinz/vim-startify'

" Vim undo tree visualizer
" pynvim must be installed !!!! : [sudo pip3 install pynvim]
Plug 'simnalamburt/vim-mundo'
nnoremap <F5> :MundoToggle<CR>

" Blazing fast minimap for vim
" Must install [code-minimap]
if executable("code-minimap")
	Plug 'wfxr/minimap.vim'
	" let g:minimap_width = 10
	let g:minimap_auto_start = 0
	let g:minimap_auto_start_win_enter = 1
endif

" SVN!!
Plug 'juneedahamed/vc.vim'

call plug#end()
"****************************** [Plug in Manger : vim-plug] ******************************


"=========================================================================================
" Essential Settings
"=========================================================================================
" set numberwidth=2
" tab size
set tabstop=4
set shiftwidth=4
" indent
set autoindent
set cindent
filetype indent on
" ignore indent when the preprocessor is typed
set smartindent
set smarttab                            " make <tab> and <backspace> smarter"
set noexpandtab                         " use tabs, not spaces"
" ---------- Highlight
set cursorline
set hlsearch
set list
set listchars=space:·,tab:→\ ,trail:•
highlight SpecialKey ctermfg=DarkGray guifg=#A0A0A0
"highlight BadWhitespace ctermbg=red guibg=darkred

"----------Search
set incsearch " show incremental search results as you type
"without capital character -> ignore
"more than one capatal character -> case sensitve
set ignorecase smartcase
set noswapfile
" save file with encoding
set fileencoding=utf-8
set encoding=UTF-8

"save key map
" nnoremap <silent><c-s> :<c-u>update<cr>
" vnoremap <silent><c-s> <c-c>:update<cr>gv
" inoremap <silent><c-s> <c-o>:update<cr>

" Clear highlight for search result				[,] -> [Space key]
nnoremap ,<space> :noh<CR>
" Trim unwanted trail space
nnoremap ,ct :%s/\s\+$//e<CR>
" Toggle line number & white space formatting, GitGutter 	[,] -> [m]

"=========================================================================================
" Color Scheme settings
"=========================================================================================
if has("syntax")
	syntax on
endif
filetype on
if has('termguicolors')
	set termguicolors
endif
set background=dark

let g:gruvbox_material_background = 'hard'
" let g:gruvbox_material_palette = 'material'
let g:gruvbox_material_palette = 'mix'
let g:gruvbox_material_transparent_background = 1
let g:gruvbox_material_enable_italic = 0
let g:gruvbox_material_disable_italic_comment = 1
let g:gruvbox_material_better_performance = 1
colorscheme gruvbox-material

" let g:gruvbox_contrast_dark='hard'
" let g:gruvbox_contrast_dark='medium'
" let g:gruvbox_contrast_dark='hard'
" let g:gruvbox_transparent_bg=1
" let g:gruvbox_italic=1
"
" Workaround for creating transparent bg
" https://stackoverflow.com/questions/37712730/set-vim-background-transparent/67569365#67569365
" autocmd SourcePost * highlight Normal     ctermbg=NONE guibg=NONE
			" \ |    highlight LineNr     ctermbg=NONE guibg=NONE
			" \ |    highlight SignColumn ctermbg=NONE guibg=NONE
" colorscheme gruvbox

"colorscheme badwolf

"colorscheme jellybeans
"let g:jellybeans_use_term_italics = 1
"colorscheme hybrid

"colorscheme sonokai
"let g:sonokai_style = 'andromeda'
"let g:sonokai_enable_italic = 1
"let g:sonokai_disable_italic_comment = 1

" Set Transparent if possible
" hi Normal guibg=NONE ctermbg=NONE


"=========================================================================================
" Using clipboard
"=========================================================================================
set clipboard=unnamedplus
vmap <C-c> "+y
map <C-v> <ESC>"+p<CR>

"=========================================================================================
" Buffer Control Key map
"=========================================================================================
" Buffer Next
map <F4> :bn<CR>
" Buffer Before
map <F3> :bp<CR>
" Buffer Delete
map ,bd :bd<CR>

if executable("code-minimap")
	nnoremap ,cm :set signcolumn=number<CR>:set number list<CR>:GitGutterEnable<CR> :Minimap<CR>
	nnoremap ,cn :set signcolumn=no<CR>:set relativenumber! number! list!<CR>:GitGutterDisable<CR> :MinimapClose<CR>
else
	nnoremap ,cm :set signcolumn=number<CR>:set number list<CR>:GitGutterEnable<CR>
	nnoremap ,cn :set signcolumn=no<CR>:set relativenumber! number! list!<CR>:GitGutterDisable<CR>
endif

"----------C code auto formatting
" NOTE : asytle is required
" auto formatting like linux kernel coding
if executable("astyle")
	nnoremap ,ca :%!astyle --style=linux --pad-oper --pad-comma
				\ --indent-switches --indent=tab --attach-return-type
				\ --break-one-line-headers --align-pointer=name --align-reference=name<CR>
	" TCC Style
	nnoremap ,cs :%!astyle --style=bsd --indent=tab --indent-switches --pad-header --pad-oper --pad-comma --align-pointer=name<CR>
	" nnoremap ,g :%!astyle --style=linux --indent-switches --pad-header --pad-oper --delete-empty-lines --indent=tab<CR>
	" autocmd BufWritePre *.h,*.hpp,*.c,*.cpp :%!astyle --style=otbs --pad-oper --delete-empty-lines --indent=tab
endif

" Compile & Run
if executable("gcc")
	" noremap <Ctrl><F5> !gcc -o hsomename % && ./somename
	nnoremap ,<F5> :!gcc -Wall % && ./a.out <CR>
	nnoremap ,<F6> :!gcc -Wall %<CR>
	nnoremap ,<F7> :!gcc -Wall -g %<CR>
	" map <F8> :w <CR> :!gcc % && ./a.out <CR>
	" map <F8> :w <CR> :!gcc % -o %< && ./%< <CR>
endif

" ----------Ctags!
" ctags
if executable("ctags")
    set tags=./tags,tags
    " set tags+=~/.stdlib.tag,/home/$USER/.stdlib.tag
    set tags+=/home/$USER/.tags/stdlib.tag
    " set tags+=/home/$USER/.tags/dbus.tag
endif

" ---------- cscope
if executable("cscope")
	function! LoadCscope()
		let db = findfile("cscope.out", ".;")
		if (!empty(db))
			let path = strpart(db, 0, match(db, "/cscope.out$"))
			set nocscopeverbose " suppress 'duplicate connection' error
			exe "cs add " . db . " " . path
			set cscopeverbose
			" else add the database pointed to by environment variable
		elseif $CSCOPE_DB != ""
			cs add $CSCOPE_DB
		endif
	endfunction

	au BufEnter /* call LoadCscope()
endi
